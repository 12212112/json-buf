#include "JsonManager.hpp"
#include <iostream>
#include <string>

int main()
{  
   // fitst part : convert a json to some smaller jsons and generate a txt file to record them path address
   std::string json_path = "../primaries.json";
   JsonManager *jsonmanager = new JsonManager;
   json json1 = jsonmanager->read_json(json_path);
   std::cout<< json1[128].dump(4)<<std::endl;
   //read json file
   jsonmanager->generator(json1, 80, 120);
   //generate the divided json file, 2 parameter, 1 is buffer length of path, 2 is the nummber of evnet in each file 
   //defult out path is "../output/"
   
   // second part : read some smaller jsons just like a file
   std::string txt_path = "../output/index.txt";
   jsonmanager->entry_map(txt_path);
   jsonmanager->set_npf(120);
   jsonmanager->initial_buf();
   std::cout<< jsonmanager->get_subjson(128).dump(4)<<std::endl;
   
   return 0;
}

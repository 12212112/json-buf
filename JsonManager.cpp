#include "JsonManager.hpp"
#include <string>

JsonManager::JsonManager()
{
}
JsonManager::~JsonManager()
{
}

json JsonManager::read_json(const std::string &para)
{
    std::ifstream i(para);
    if (!i.is_open())
    {
        std::cerr << "read_json() Could not open the file - '"
                  << para << "'" << std::endl;
    }
    json j;
    i >> j;
    return j;
}

void JsonManager::generator(json &j, int pathbuf, int num)
{
    int len = j.size();
    int file_num = len / num + 1;
    char str[pathbuf];
    std::ofstream index("../output/index.txt");
    for (size_t i = 0; i < file_num; i++)
    {
        sprintf(str, "../output/%zu.json", i);
        index << str << std::endl;
        std::ofstream o(str);
        json subjson;
        for (size_t k = 0; (k < num) && (i * num + k < len); k++)
        {
            subjson += j[i * num + k];
        }
        o << subjson.dump(4);
    }
}

void JsonManager::entry_map(const std::string &para)
{
    std::ifstream file(para);
    if (!file.is_open())
    {
        std::cerr << "entry_map() Could not open the file - '"
                  << para << "'" << std::endl;
    }
    std::string str;
    int counter = 0;
    while (std::getline(file, str))
    {
        maplist.insert({counter, str});
        counter += 1;
    }
}

void JsonManager::initial_buf()
{   
    std::string filename = maplist.at(0);
    std::ifstream i(filename);
    if (!i.is_open())
    {
        std::cerr << "initial_buf() Could not open the file - '"
                  << filename << "'" << std::endl;
    }
    json j;
    i >> j;
    json_buf.insert({0, j});
}

json JsonManager::get_subjson(int num)
{   
    int filenumber = num / npf;
    
	auto iter = json_buf.find(filenumber);
	if (iter != json_buf.end())
	{
		return json_buf.at(filenumber)[num % npf];
	}
	else
	{
        re_buf(filenumber);
		return json_buf.at(filenumber)[num % npf];
	}
}

void JsonManager::re_buf(int filenum)
{ 
    json_buf.clear();
    std::string filename = maplist.at(filenum);
    std::ifstream i(filename);
    if (!i.is_open())
    {
        std::cerr << "re_buf() Could not open the file - '"
                  << filename << "'" << std::endl;
    }
    json j;
    i >> j;
    json_buf.insert({filenum, j});
} 

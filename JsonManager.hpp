#pragma once

#include "nlohmann/json.hpp"
#include "vector"
#include <fstream>
#include <stdio.h>
#include <string>
#include <iostream>

using json = nlohmann::json;

class JsonManager
{
public:

  JsonManager();
  ~JsonManager();
  json read_json(const std::string& para);
  // read json file
  void generator(json &j, int pathbuf, int num);
  // genetate sub-json file and index.txt which saved the address
  // parameter pathbuf is the buffer length of path
  // parameter num is the number of event each sub-json file 
  void entry_map(const std::string& para);
  // input index.txt and entry maplist
  std::map<int, std::string>& get_maplist(){return maplist;};
  void set_npf(int num){npf = num;};
  //set the number of event each sub-json file needed to read
  void initial_buf();
  json get_subjson(int num);
  //return the event in sub-json file and 
  void re_buf(int filenum);
private:

  std::map<int, std::string> maplist;
  std::map<int, json> json_buf;
  int npf;
};

